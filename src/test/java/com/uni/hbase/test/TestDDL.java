package com.uni.hbase.test;

import com.uni.hbase.utils.HbaseConn;
import com.uni.hbase.utils.HbaseDDL;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Table;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/*
 * @Author: Uni
 * @Time: 2022/5/4
 * @TODO
 */
public class TestDDL {


    /**
     * 测试: 创建一个命名空间，并判断是否存在
     * 测试方法:
     *          {@link HbaseDDL#createNamespace(String)}
     *          {@link HbaseDDL#existedNamespace(String)}
     */
    @Test
    public void testCreateNamespace() {
        String testNamespace = "uni";
        System.out.println("[TEST] >> 创建了命名空间: " + testNamespace);
        HbaseDDL.createNamespace(testNamespace);
        System.out.println("[INFO] >> 命名空间【" + testNamespace + "】已存在的判断结果为: "
                                + HbaseDDL.existedNamespace(testNamespace));
    }

    /**
     * 测试: 获取所有命名空间的信息
     */
    @Test
    public void testListNamespace(){
        System.out.println("[TEST] >> 查询所有命名空间的结果为: ");
        Map<String, Map<String, String>> map = HbaseDDL.listNamespaceName();
        int i = 1;
        for (Map.Entry<String, Map<String, String>> entry : map.entrySet()) {
            System.out.println(i+": -------------------------------------");
            System.out.println("--命名空间名称:" + entry.getKey());
            System.out.println("--命名空间配置:");
            for (Map.Entry<String, String> prop : entry.getValue().entrySet()) {
                System.out.println("---- name: " + prop.getKey() + " ||  value: " + prop.getValue());
            }
            System.out.println("---------------------------------------");
            i++;
        }
    }
    @Test
    public void testModifyNamespace(){
        Map<String, String> prop = new HashMap<>();
        prop.put("name", "uni");
        prop.put("age", "21");
        HbaseDDL.modifyOneNamespace("uni", prop);
    }
    @Test
    public void testDescNamespace(){
        String testNamespace = "uni";
        Map<String, String> map = HbaseDDL.descNamespace(testNamespace);
        System.out.println("[TEST] 查询指定命名空间【" + testNamespace +"】的结果为: ");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ": " +  entry.getValue());
        }
    }

    @Test
    public void testDeleteNamespace(){
        String testNamespace = "uni";
        System.out.println("[TEST] 测试删除命名空间: 【 " + testNamespace + " 】, 结果为: "+
                HbaseDDL.deleteNamespace(testNamespace));
    }


    /**
     * 扫描所有的表
     */
    @Test
    public void testScanTableNames(){
        System.out.println("[TEST] >> 扫描所有的表, 结果为:"
            + HbaseDDL.scanTables()
        );
    }

    @Test
    public void testTableExists(){
        String testTableName = "uni";
        boolean result = HbaseDDL.tableExists(testTableName);
        System.out.println("[INFO] >> 判断表 [" + testTableName + "] 存在的结果为: " + result);
    }

    /**
     * 测试: 使用 返回值 Table 和 boolean 这两种不同的创建表方法, 查看方法是否有效
     * @throws TableExistsException 创建表时[表已经存在]抛出的异常
     */
    @Test
    public void testCreateTable() throws TableExistsException {
        String testTableName1 = "uni1";
        String testTableName2 = "uni2";
        boolean existed = HbaseDDL.tableExists("testTableName");
        System.out.println("[INFO] >> 判断表 uni1 存在的结果为: " + existed);
        if(!existed){
            Table table = HbaseDDL.createTableAndNeed(testTableName1, "info1", "info2");
            System.out.println("[TEST] >> 创建表1成功, 表名为: " + table.getName());
        }
        System.out.println("[TEST] >> 创建表2的结果为: " +
                HbaseDDL.createTable(testTableName2,"info1", "info2"));
    }
}
