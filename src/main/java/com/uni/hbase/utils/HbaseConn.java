package com.uni.hbase.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/*
 * @Author: Uni
 * @Time: 2022/5/4
 * @TODO 获取 HBase 表的连接
 */
public class HbaseConn {
    private static Connection conn = null;
    private static Admin admin = null;
    private static String ZK_KEY = "hbase.zookeeper.quorum";
    private static String ZK_VALUE = "hadoop101,hadoop102,hadoop103";
    // 单例 + 工厂模式
    static {
        Configuration conf = new Configuration(true);
        try {
            // 配置 zookeeper, 用于连接 HBase
            conf.set(ZK_KEY, ZK_VALUE);
            conn = ConnectionFactory.createConnection(conf);

            // 获取 管理员对象
            admin = conn.getAdmin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
     *  提供一些方法 获取或释放连接
     */
    public static Connection getConn(){ return conn; }
    public static Admin getAdmin() { return admin; }

    public static void close(){
        try{
            if(admin != null) admin.close();
            if(conn != null) conn.close();
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
